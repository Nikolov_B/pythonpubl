# coding: utf8
'''Универсальный консольный скрипт  для магнитных обсерваторий, позволяющий автоматизировать пересылку файлов на FTP - сервер, архивацию данных и создание бэкапов.
По умолчанию работает со вчерашними данными, может также работать с любой датой.'''

import argparse
import sys
import datetime as dt
import os
import zipfile
import logging
import re
import ftplib

#определение лог файла
logger = logging.getLogger("FileTransfer.py")
logger.setLevel(logging.INFO)
fh = logging.FileHandler("FileTransfer.log")
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

#парсер входных параметров
def create_parser():
    parser = argparse.ArgumentParser(prog='FileTranfer')

    #трехбуквенный код обсерватории
    parser.add_argument('-c', '--code', required=True, help='Observatory code (3 characters)')

    #папка с файлам данных
    parser.add_argument('-d', '--data', required=True, help='Directory containing data')

    #папка для бэкапов
    parser.add_argument('-b', '--backup', required=True, help='Directory for backups')

    #папка для отправки данных
    parser.add_argument('-t', '--transfer', required=True, help='Directory for transfer data')

    #директория на сервере
    parser.add_argument('-s', '--servdir', required=True, help='Directory for files on ftp-server')

    #Дата в формате yy-mm-dd
    parser.add_argument('-ymd', '--date', default=yesterday(), required=False, help='Date in format %Y-%m-%d')

    #Ключ для сегодняшней даты
    parser.add_argument('-td', '--today', action='store_true', help='True or False for today files')

    #ftp-server
    parser.add_argument('-f', '--ftp', required=False, help='ftp-server')

    #username
    parser.add_argument('-u', '--username', required=False, help='Username on ftp')

    #password
    parser.add_argument('-p', '--password', required=False, help='Password on ftp')
    return parser

def today():
    return dt.datetime.now().date()

#вчерашняя дата
def yesterday():
    return today()-dt.timedelta(days=1)


#Дата из строки
def YMD_from_string(str):
    try:
        date = dt.datetime.strptime(str, "%Y-%m-%d")
        logger.info("Date %s successfully parsed", str)
        return date
    except ValueError:
        logger.error("Cannot parse DATE %s", str)
        sys.exit()


#номер дня года
def day_number(date):
    return date.timetuple().tm_yday


#существует ли директория
def dir_exist(dirPath):
    return os.path.isdir(dirPath)


#проверка есть ли хоть 1 файл для работы
def check_files(files):
    if len(files) == 0:
        logger.error("No files to perform the operation")
        sys.exit()


#получение списка файлов
def get_files(data_folder, observatory_code, datetimeYDM):
    exist_files = []
    if dir_exist(data_folder):
        files = []
        # файл 1
        if datetimeYDM != today():
            file1 = observatory_code + str(datetimeYDM.year)+"." + str(day_number(datetimeYDM))
        else:
            file1 = observatory_code + str(datetimeYDM.year) + ".pmb"
        files.append(file1)
        # файлы 2 и 3

        pattern = observatory_code + dt.datetime.strftime(datetimeYDM, "%Y%m%d")
        pattern = pattern.lower()
        catalogFile = os.listdir(data_folder)
        for f in catalogFile:
            result = re.match(pattern, f.lower())
            if result != None:
                result2 = re.search('v.sec', f.lower())
                result3 = re.search('v.min', f.lower())
                if result2!=None or result3!=None:
                    files.append(f)

        files = [os.path.join(data_folder, f) for f in files]

        for f in files:
            if os.path.isfile(f):
                exist_files.append(f)
            else:
                logger.warning("File %s is not exist", f)
    else:
        logger.error("Directory %s is not exist ", data_folder)
    return exist_files


#создание бекапа
def archive(backupDir, files, observatory_code, datetimeYDM):
    logger.info("Start of Backup creation")
    if dir_exist(backupDir):
        name = observatory_code + dt.datetime.strftime(datetimeYDM, "%Y%m%d") + ".zip"
        name = os.path.join(backupDir, name)
        try:
            z = zipfile.ZipFile(name, 'w', zipfile.ZIP_DEFLATED)
            logger.info("Archive created - " + str(name))
            for f in files:
                try:
                    z.write(f, os.path.basename(f))
                    logger.info("File %s successfully added to archive", f)
                except Exception:
                    logger.error("File %s could not be added to archive", f)
        except OSError:
            logger.error("Failed to create an archive")
    else:
        logger.error("Directory %s is not exist ", backupDir)
    logger.info("Backup creation finished")
    return name


#отправка и удаление 1 файла
def send_file(file, con):
    try:
        f = os.path.basename(file)
        logger.info("Definition of the year %s", f)
        result = re.findall(r'\d{4}', f)
        year = result[0]
        logger.info("Year of data - %s", year)
    except ValueError:
        logger.error("Could not define year %s", f)
    else:
        path_to_save = con.pwd() +'/'+ str(year)
        try:
            logger.info("Path to save - %s", path_to_save)
            logger.info("Going to - %s", path_to_save)
            con.cwd(path_to_save)
            logger.info("Successfully!")
        except ftplib.all_errors:
            logger.info("Directory not exist, creating and going...")
            con.mkd(path_to_save)
            con.cwd(path_to_save)
    try:
        uploadFile = open(file, "rb")
        con.storbinary("STOR " + os.path.basename(file), uploadFile)
        uploadFile.close()
    except ftplib.all_errors:
        logger.error("Failed to an upload file %s", file)
        uploadFile.close()
    else:
        logger.info("File %s successfully uploaded", os.path.basename(file))
        try:
            logger.info("Deleting a copy from %s", os.path.dirname(file))
            os.remove(file)
            logger.info("File %s successfully deleted", file)
        except OSError:
            logger.error("Failed to delete file %s", file)


#отправка файлов на ftp
def send_files(transf, directoryFTP, address, user, password):
    logger.info("Start transferring files to the %s", address)
    try:
        logger.info("Checking files in the directory %s", transf)
        files = os.listdir(transf)
        files = [os.path.join(transf, f) for f in files]
        logger.info("Count of files - %s", len(files))
    except ValueError:
        logger.error("Transfer error")
    try:
        logger.info("Trying connection...")
        con = ftplib.FTP(address, user, password)
        logger.info("Successfully!")
        logger.info("Going to the directory %s", directoryFTP)
        try:
            con.cwd(directoryFTP)
            if type(files) == list:
                for f in files:
                    send_file(f, con)
                    con.cwd(directoryFTP)
            else:
                send_file(files, con)
        except ftplib.all_errors:
            logger.error("Error in moving to the directory")
        con.close()
    except ftplib.all_errors:
        logger.error("Could not connect to %s", address)
    logger.info("End of file transfer")



#Главная функция
if __name__ == '__main__':
    logger.info("#######################################################Script started!#######################################################")
    parser = create_parser()
    args = parser.parse_args(sys.argv[1:])

    ###########################################################
    code = args.code
    data = args.data
    backup = args.backup
    transf = args.transfer
    directoryFTP = args.servdir
    address = args.ftp
    user = args.username
    password = args.password
    isToday = args.today
    YMD = args.date
    if isToday:
        YMD = today()
    else:
        if YMD != yesterday():
            YMD = YMD_from_string(YMD).date()
    logger.info("Script running for date %s", YMD.strftime('%Y-%m-%d'))
    ###########################################################

    files = get_files(data, code, YMD)

    check_files(files)
    backup1 = archive(backup, files, code, YMD)

    check_files(files)
    backup2 = archive(transf, files, code, YMD)

    check_files(files)
    send_files(transf, directoryFTP, address, user, password)

    logger.info("#######################################################End of script#######################################################")